from math import pow
from typing import List
from os import environ
from pychpp import CHPP
import csv
from json import JSONEncoder, dumps


def get_league_size(level: int):
    return int(pow(4, level - 1)) if level <= 6 else 1024


class LeagueLevel:
    def __init__(self, level: int, first_id: int, *args, **kwargs):
        super(LeagueLevel, self).__init__(*args, **kwargs)
        self.level = level
        self.firstId = first_id
        self.size = get_league_size(level)


class CountryLeagues:
    def __init__(self, league_id: int, leagues: List[LeagueLevel], chpp: CHPP, *args, **kwargs):
        super(CountryLeagues, self).__init__(*args, **kwargs)
        self.leagueId = league_id
        self.leagues = leagues
        self.countryName = chpp.world(
            ht_id=league_id).league(ht_id=league_id).league_name


class BasicEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__


def generate_countries():
    consumer_key = environ.get('HT_CONSUMER_KEY')
    consumer_secret = environ.get('HT_CONSUMER_SECRET')
    access_token_key = environ.get('HT_ACCESS_TOKEN_KEY')
    access_token_secret = environ.get('HT_ACCESS_TOKEN_SECRET')
    chpp = CHPP(consumer_key, consumer_secret,
                access_token_key, access_token_secret)

    countries = []
    l_id = 1
    with open('leagueLevelUnitIDs.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        leagues = []
        for row in reader:
            league_id, level, serie_id = int(row['\ufeffLeagueId']), int(
                row['LeagueLevel']), int(row['LeagueLevelUnitId'])
            if league_id != l_id:
                print(f"\rProcessing league: #{league_id}", end='', flush=True)
                # Generate a country and reset var
                countries.append(CountryLeagues(l_id, leagues, chpp))
                leagues = []
                l_id = league_id

            leagues.append(LeagueLevel(level, serie_id))
        countries.append(CountryLeagues(l_id, leagues, chpp))

    return countries


countries = generate_countries()

countriesJSONData = dumps(countries, indent=2, cls=BasicEncoder)

with open('countries.json', 'w') as file:
    file.write(countriesJSONData)
